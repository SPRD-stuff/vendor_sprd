define generate-sprd-userimage-prop-dictionary
$(if $(BOARD_PERSISTIMAGE_PARTITION_SIZE),$(hide) echo "persist_size=$(BOARD_PERSISTIMAGE_PARTITION_SIZE)" >> $(1))
endef



# -----------------------------------------------------------------
#persist partition image
ifdef BOARD_PERSISTIMAGE_PARTITION_SIZE
INSTALLED_PERSISTIMAGE_TARGET := $(PRODUCT_OUT)/persist.img
define build-persistimage-target
  $(info Target persist fs image: $(INSTALLED_PERSISTIMAGE_TARGET))
  @dd if=/dev/zero of=$(INSTALLED_PERSISTIMAGE_TARGET) bs=1024 count=2048
  $(hide) $(call assert-max-image-size,$(INSTALLED_PERSISTIMAGE_TARGET),$(BOARD_PERSISTIMAGE_PARTITION_SIZE))
endef
$(INSTALLED_PERSISTIMAGE_TARGET): $(INTERNAL_USERIMAGES_DEPS)
	$(build-persistimage-target)

.PHONY: persistimage-nodeps
persistimage-nodeps: | $(INTERNAL_USERIMAGES_DEPS)
	$(build-persistimage-target)

droidcore: $(INSTALLED_PERSISTIMAGE_TARGET)

endif # BOARD_PERSISTIMAGE_PARTITION_SIZE
