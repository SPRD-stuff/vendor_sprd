#!/usr/bin/env python
#
# Copyright (C) 2018 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Usage: build_super_mo_image input_dir output_dir

input_dir: dir which has compiled sparse superimg

output_dir: dir which metadata only img be generated
"""

from __future__ import print_function

import logging
import os.path
import sys
import tempfile
import struct

sys.path.append('./build/tools/releasetools')
import common

if sys.hexversion < 0x02070000:
  print("Python 2.7 or newer is required.", file=sys.stderr)
  sys.exit(1)

logger = logging.getLogger(__name__)

def GetIfSparseImg(imgdir):
  f = open(imgdir,"r")
  header_bin = f.read(28)
  header = struct.unpack("<I4H4I", header_bin)
  magic_number = header[0]
  f.close()
  if magic_number == 0xED26FF3A:
    return True
  else:
    return False

def BuildMoSuperImage(inp, out):

  super_img_path = os.path.join(inp, "super.img")
  super_metadata_only_img_path = os.path.join(out, "IMAGES/super_mo.img")

  super_rawimg_path = super_img_path

  is_super_sparse = GetIfSparseImg(super_img_path)

  if is_super_sparse:
    print("OUT's Super img is sparse...")
    super_rawimg = tempfile.NamedTemporaryFile()
    cmd = ["simg2img", super_img_path, super_rawimg.name]
    common.RunAndCheckOutput(cmd)
    super_rawimg_path = super_rawimg.name
  else:
    print("OUT's Super img is raw...")

  f_superimg = open(super_rawimg_path, "rb")
  metadata_data = f_superimg.read(65536)

  f_mo_img = open(super_metadata_only_img_path, "w+b")
  f_mo_img.write(metadata_data)

  if is_super_sparse:
    super_rawimg.close()
  f_superimg.close()
  f_mo_img.close()

def main(argv):

  args = common.ParseOptions(argv, __doc__)

  if len(args) != 2:
    common.Usage(__doc__)
    sys.exit(1)

  common.InitLogging()

  BuildMoSuperImage(args[0], args[1])


if __name__ == "__main__":
  try:
    common.CloseInheritedPipes()
    main(sys.argv[1:])
  except common.ExternalError:
    logger.exception("\n   ERROR:\n")
    sys.exit(1)
  finally:
    common.Cleanup()
