#!/usr/bin/env python
#
# Copyright (C) 2018 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
A tool for extracting and printing time cost info from build logs at the end of the build.
"""

import datetime
import gzip
import os
import re
import shutil
import sys

usage = """
Usage:
argv version 1: 'build_analysis.py out_dir --copy' to copy build logs
argv version 2: 'build_analysis.py out_dir build_cmd' to analysis build logs
"""
if len(sys.argv) < 3:
  print usage
  sys.exit(1)

# print colors
CRED = '\033[91m'
CGREEN = '\033[32m'
CYELLOW = '\033[33m'
CEND = '\033[0m'

_OUT_DIR = sys.argv[1]
_LAST_BUILD_DIR = _OUT_DIR + '/' + 'lastbuild'
_ERROR_LOG = 'error.log'
_SOONG_LOG = 'soong.log'
_NINJA_LOG = '.ninja_log'
_VERBOSE_LOG = 'verbose.log.gz'
_BUILD_TRACE = 'build.trace.gz'
_BUILD_RECORD = 'build_record.txt'
_LAST_BUILD_LOGS = [
  _ERROR_LOG,
  _SOONG_LOG,
  _NINJA_LOG,
  _VERBOSE_LOG,
  _BUILD_TRACE,
  _BUILD_RECORD
]


def copy_last_build_logs():
  """Copy all relative logs to out/lastbuild."""
  if os.path.exists(_LAST_BUILD_DIR):
    shutil.rmtree(_LAST_BUILD_DIR)
  os.mkdir(_LAST_BUILD_DIR)
  for log in _LAST_BUILD_LOGS:
    try:
      # mm/mmm/mma/mmma/m will not start bsp build
      # so move build_record.txt instead of copy
      if log == _BUILD_RECORD:
        shutil.move(_OUT_DIR + '/' + log, _LAST_BUILD_DIR + '/' + log)
      else:
        shutil.copyfile(_OUT_DIR + '/' + log, _LAST_BUILD_DIR + '/' + log)
    except IOError as e:
      print 'Failed to copy ' + log


def parse_trace_and_record_files():
  """Parse trace and record files for build timestamps."""
  try:
    with gzip.open(_LAST_BUILD_DIR + '/' + _BUILD_TRACE, 'rt') as f:
      trace_buff = f.read()
  except IOError as e:
    print CYELLOW + 'Failed to open ' + _BUILD_TRACE + CEND
    return

  record_buff = None
  try:
    with open(_LAST_BUILD_DIR + '/' + _BUILD_RECORD, 'r') as f:
      record_buff = f.read()
  except IOError as e:
    print CYELLOW + 'Failed to open ' + _BUILD_RECORD + CEND

  """
  process trace timestamp from the line with "pid":0 and "ph":"B" or "E"

  e.g.
  {"name":"kati build","ph":"B","ts":1565241890996645,"pid":0,"tid":0},
  {"ph":"E","ts":1565241918404680,"pid":0,"tid":0},

  "name": the subprocess name
  "B": start time
  "E": end time

  it will be record as ("kati build", 1565241918404680 - 1565241890996645)
  use a stack to deal with nested situation like 'BBEBEE'
  """
  traces = []
  stack = []
  start_time = 0
  for line in trace_buff.splitlines():
    if '"pid":0' in line:
      content = re.findall(r'^\{(.*?)\}[,|\]]', line)
      if content != []:
        fields = content[0].split(',')
        name = ''
        time = 0
        duration = 0
        push = False
        skip = False

        for field in fields:
          pair = field.split(':')
          if skip:
            break;
          if pair[0] == '"name"':
            name = pair[1].replace('"', '')
          if pair[0] == '"ph"':
            if pair[1] == '"B"':
              push = True
            if pair[1] == '"M"':
              skip = True
          if pair[0] == '"ts"':
            time = float(pair[1])
          if pair[0] == '"dur"':
            duration = float(pair[1])

        if skip:
          continue

        if start_time == 0 and not skip:
          start_time = time

        if duration != 0:
          traces.append((name, duration))
        else:
          if push:
            stack.append((name, time))
          else:
            if stack != []:
              begin = stack.pop()
              # add prefix for sub-subprocesses, like [soong]bootstrap
              prefix = ''
              for s in stack:
                prefix += '[' + s[0] + ']'
              traces.append((prefix + begin[0], time - begin[1]))
            else:
              print CYELLOW + 'Parse failed, build.trace.gz may be wrong format' + CEND

  # build time stats
  if traces != [] and stack == []:
    total_time = android_build_time = time - start_time
    bsp_build_time = 0
    sign_images_time = 0
    real_build_time = traces[-1][1] if traces[-1][0] == 'ninja' else 0
    regen_time = android_build_time - real_build_time

    # parse build record for bsp build & sign images time
    if record_buff is not None:
      for line in record_buff.splitlines():
        fields = line.split(':')
        name = fields[0]
        interval = float(fields[1]) * 1000000
        total_time += interval
        if name == 'bsp_build':
          bsp_build_time = interval
        if name == 'sign_images':
          sign_images_time = interval

    print CGREEN + '[total build time '\
        + str(datetime.timedelta(seconds=round(total_time/1000000)))\
        + ' (hh:mm:ss)]' + CEND
    if bsp_build_time > 0:
      print 'bsp build time: '\
          + str(datetime.timedelta(seconds=round(bsp_build_time/1000000)))\
          + '(' + '{:.2%}'.format(bsp_build_time/total_time) + ')'
    print 'android build time: '\
        + str(datetime.timedelta(seconds=round(android_build_time/1000000)))\
        + '(' + '{:.2%}'.format(android_build_time/total_time) + ')'
    output = '\t[android build]regen: '\
        + str(datetime.timedelta(seconds=round(regen_time/1000000)))\
        + '(' + '{:.2%}'.format(regen_time/android_build_time) + ')'
    # red warning when regen time > 10 min
    if regen_time > 600000000:
      output = CRED + output + CEND
    print output
    print '\t[android build]real build: '\
        + str(datetime.timedelta(seconds=round(real_build_time/1000000)))\
        + '(' + '{:.2%}'.format(real_build_time/android_build_time) + ')'
    if sign_images_time > 0:
      print 'sign images time: '\
          + str(datetime.timedelta(seconds=round(sign_images_time/1000000)))\
          + '(' + '{:.2%}'.format(sign_images_time/total_time) + ')'
    # android build time details
    print CGREEN + '[android build time details]' + CEND
    for trace in traces:
      output = trace[0] + ': ' + '{:0,.2f}'.format(trace[1]/1000) + 'ms'\
        + '(' + '{:.2%}'.format(trace[1]/total_time) + ')'
      # red warning for subprocesses which cost > 5 min
      if trace[1] > 300000000:
        output = CRED + output + CEND
      print output
  else:
    print CYELLOW + 'Parse failed, build.trace.gz may be empty or wrong format' + CEND


def parse_verbose_file():
  """Parse verbose file for regen reasons."""
  try:
    with gzip.open(_LAST_BUILD_DIR + '/' + _VERBOSE_LOG, 'rt') as f:
      verbose = f.read()
  except IOError as e:
    print CYELLOW + 'Failed to open ' + _VERBOSE_LOG + CEND
    return

  # regen reasons
  regen_reasons = []
  for line in verbose.splitlines():
    if 'regenerating...' in line:
      regen_reasons.append(line)
  if regen_reasons != []:
    print CGREEN + '[ninja regen reason]' + CEND
    for i, r in enumerate(regen_reasons, start=1):
      print CRED + str(i) + ": " + r + CEND


def main(argv):
  """Program entry point."""
  print '===============BUILD TIME ANALYSIS==============='
  parse_verbose_file()
  parse_trace_and_record_files()
  # command that starts the build
  print CGREEN + '[build command]' + CEND
  print ' '.join(argv)
  print '===============BUILD TIME ANALYSIS==============='

if __name__ == '__main__':
  if len(sys.argv) == 3 and sys.argv[2] == '--copy':
    copy_last_build_logs()
  else:
    main(sys.argv[2:])