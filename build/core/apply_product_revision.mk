# Spreadtrum Communication Inc.

ifneq ($(wildcard vendor/sprd/feature_configs),)

# This make file must be included after defining BOARDDIR and PLATDIR

# Here is a common config dir now
PRODUCT_REVISION_COMMON_CONFIG_PATH ?= vendor/sprd/feature_configs

#include $(wildcard $(PRODUCT_REVISION_COMMON_CONFIG_PATH)/feature_retain_configs.mk)
include $(wildcard $(PRODUCT_REVISION_COMMON_CONFIG_PATH)/carrier_configs.mk)

carrier_feature_dir := carriers

FEATURES.CARRIERS += $(shell ls $(PRODUCT_REVISION_COMMON_CONFIG_PATH)/$(carrier_feature_dir))

ifeq ($(strip $(PRODUCT_REVISION)),)
# Don't warning, only show nothing to include
# $(warning You should define the PRODUCT_REVISION before you call include APPLY_PRODUCT_REVISION)
endif

# If you want to point to another folder to control the features, declare
# the PRODUCT_FEAULTE_LIST_PATH
ifneq ($(strip $(PRODUCT_FEAULTE_LIST_PATH)),)

PRIVATE_FEATURE_LIST_PATH := $(PRODUCT_FEAULTE_LIST_PATH)

else

PRIVATE_FEATURE_LIST_PATH := \
        $(BOARDDIR)/features/$(carrier_feature_dir) $(BOARDDIR)/features \
        $(PLATDIR)/features/$(carrier_feature_dir)  $(PLATDIR)/features \
        $(PLATDIR)/common/features/$(carrier_feature_dir) $(PLATDIR)/common/features \
        $(PRODUCT_REVISION_COMMON_CONFIG_PATH)/$(carrier_feature_dir) $(PRODUCT_REVISION_COMMON_CONFIG_PATH)
endif

# PRODUCT_FEATURE_LIST_PATH must put higher prority at first and then
# we adjust them for our applying.
PRIVATE_ADJUSTED_FEATURE_LIST_PATH :=
$(foreach path,$(PRIVATE_FEATURE_LIST_PATH), \
    $(eval PRIVATE_ADJUSTED_FEATURE_LIST_PATH := $(path) $(PRIVATE_ADJUSTED_FEATURE_LIST_PATH)) \
)
PRIVATE_FEATURE_LIST_PATH := $(PRIVATE_ADJUSTED_FEATURE_LIST_PATH)

# If not define the PRODUCT_DEFAULT_REVISION, give the base to default
ifeq (,$(strip $(PRODUCT_DEFAULT_REVISION)))
PRODUCT_DEFAULT_REVISION := base
endif

# If not define the PRODUCT_REVISION, use default one.
ifeq ($(strip $(PRODUCT_REVISION)),)
PRODUCT_REVISION := $(PRODUCT_DEFAULT_REVISION)
endif

# If hudson or the builder define a revision, override configs before.
ifdef PRODUCT_SET_REVISION
#PRIVATE_FEATURE_REVISION := $(PRODUCT_SET_REVISION)
#$(foreach feature,$(PRODUCT_REVISION), \
#    $(eval $(if $(filter true,$(FEATURES.$(feature).CONFIG.RETAIN)),\
#        PRIVATE_FEATURE_REVISION += $(feature),)) \
#)

$(foreach feature,$(FEATURES.CARRIERS), \
    $(eval PRODUCT_REVISION := $(filter-out $(feature),$(PRODUCT_REVISION))) \
)

PRIVATE_FEATURE_REVISION := $(PRODUCT_REVISION) $(PRODUCT_SET_REVISION)
# If base was found, remove base and re-ordered base to front
ifneq ($(filter base,$(PRIVATE_FEATURE_REVISION)),)
PRIVATE_FEATURE_REVISION := base $(filter-out base,$(PRIVATE_FEATURE_REVISION))
endif

else #PRODUCT_SET_REVISION
PRIVATE_FEATURE_REVISION := $(PRODUCT_REVISION)
endif #PRODUCT_SET_REVISION is set

# If no including for base, add base features
ifeq (,$(strip $(filter base,$(PRIVATE_FEATURE_REVISION))))
PRIVATE_FEATURE_REVISION := base $(PRIVATE_FEATURE_REVISION)
endif

# $(warning _just_for_additionals is $(_just_for_additionals))
ifneq ($(strip $(_just_for_additionals)),true)
_just_for_additionals :=
endif

ifndef _just_for_additionals
$(warning -------------------------------------------------)
$(warning PRODUCT_REVISION : "$(PRIVATE_FEATURE_REVISION)")
$(warning -------------------------------------------------)
endif

# Debug
#$(warning DEBUG PRIVATE_FEATURE_LIST_PATH = $(PRIVATE_FEATURE_LIST_PATH))
#$(warning DEBUG PRIVATE_FEATURE_REVISION = $(PRIVATE_FEATURE_REVISION))

PRIVATE_FEATURE_MODULES :=
# Scan PLATDIR and BOARDDIR features folder and check wether they exists.
PRIVATE_FEATURE_MODULES :=

$(foreach feature,$(PRIVATE_FEATURE_REVISION), \
  $(foreach path,$(PRIVATE_FEATURE_LIST_PATH), \
     $(eval PRIVATE_FEATURE_MODULES := $(wildcard $(path)/$(feature)/config.mk) $(PRIVATE_FEATURE_MODULES) \
     ) \
  ) \
)

ifneq ($(wildcard $(strip $(PRIVATE_FEATURE_MODULES))),)
# Debug
# $(warning including $(PRIVATE_FEATURE_MODULES))

### RAW INCLUDE: ###
# include exists config.mk files.
include $(PRIVATE_FEATURE_MODULES)

#$(info PRIVATE_FEATURE_MODULES := $(PRIVATE_FEATURE_MODULES))

else
$(warning Failed to apply any modules)
endif # PRIVATE_FEATURE_MODULES exists

# In definitions.mk -> vendor/*/definitions.mk, we should never apply _product_var_list
# features, so just go including to apply ADDITIONAL_(DEFAULT_)BUILD_PROPERTIES
ifndef _just_for_additionals
##### After including the features, use the results to do something more.

# The addon packages is equal to PRODUCT_PACKAGES, combine them.
# TODO In future, they may be put into another partition
FEATURES.PRODUCT_PACKAGES += $(FEATURES.PRODUCT_ADDON_PACKAGES)

# Debug
# $(foreach v,$(_product_var_list), \
#    $(eval $(warning FEATURES.$(v) ->) $(warning $(FEATURES.$(v))) ))

# TODO, the OEM property should use a delegate to control them, that may be much different currently
ifdef BOARD_OEMIMAGE_PARTITION_SIZE
# TODO, should generate the delegate. And try to put into the oem.prop
property_overrides := PRODUCT_OEM_PROPERTIES
else
# TODO, append them into ADDITIONAL_BUILD_PROPERTIES
property_overrides := PRODUCT_PROPERTY_OVERRIDES
endif

raw_product := PRODUCTS.$(INTERNAL_PRODUCT)

_unsupported_revision_var_list := PRODUCT_ADB_KEYS
_supported_revision_var_list := $(_product_var_list)

$(foreach v,$(_unsupported_revision_var_list), \
    $(eval _supported_revision_var_list := $(filter-out $(v),$(_supported_revision_var_list))) \
)

# Foreach FEATURES.PRODUCT* and add them to PRODUCT* currently.
# TODO In future, they may be put into another parition
$(foreach v,$(_supported_revision_var_list), \
    $(eval $(v) := $(FEATURES.$(v)) $($(v))) \
    $(eval $(raw_product).$(v) := $(FEATURES.$(v)) $($(raw_product).$(v))) \
)

# To control the oem.prop
# ifndef BOARD_OEMIMAGE_PARTITION_SIZE
# The properties has been set into the ADDITIONAL_BUILD_PROPERTIES, so append them.
# ADDITIONAL_BUILD_PROPERTIES += $(FEATURES.PRODUCT_PROPERTY_OVERRIDES)
# $(info ADDITIONAL_BUILD_PROPERTIES := $(ADDITIONAL_BUILD_PROPERTIES))
# endif

# The Locales will be overwrite completely
ifdef FEATURES.PRODUCT_LOCALES
PRODUCT_AAPT_CONFIG := $(strip $(filter-out $(PRODUCTS.$(INTERNAL_PRODUCT).PRODUCT_LOCALES),$(PRODUCT_AAPT_CONFIG)))
PRODUCT_LOCALES := $(strip $(FEATURES.PRODUCT_LOCALES))
PRODUCT_AAPT_CONFIG := $(strip $(PRODUCT_LOCALES) $(PRODUCT_AAPT_CONFIG))
#$(info PRODUCT_LOCALES := $(PRODUCT_LOCALES))
endif

#$(info )

# Debug
#$(warning PRODUCT_PACKAGES after including $(PRODUCT_PACKAGES) PRODUCT_BOOT_JARS -> $(PRODUCT_BOOT_JARS))

ADDITIONAL_DEFAULT_PROPERTIES :=
ADDITIONAL_BUILD_PROPERTIES :=
#$(warning reseting additional properties $(ADDITIONAL_BUILD_PROPERTIES))

endif # _just_for_additionals

endif # vendor/sprd/feature_configs exists

# clear vars
PRIVATE_FEATURE_MODULES :=
PRIVATE_FEATURE_LIST_PATH :=
PRIVATE_FEATURE_REVISION :=
