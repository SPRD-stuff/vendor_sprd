#ifndef __SPRD_3DNR_ADAPTER_LOG_H__
#define __SPRD_3DNR_ADAPTER_LOG_H__

#include <android/log.h>


#define  LOGI(...)  __android_log_print(ANDROID_LOG_DEBUG,"3DNR_ADAPT_LOG",__VA_ARGS__)

#endif
