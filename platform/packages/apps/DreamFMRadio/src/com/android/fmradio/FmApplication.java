package com.android.fmradio;

import com.android.fmradio.CurrentStationItem;
import android.content.Context;
import android.app.Application;

public class FmApplication extends Application {

    private Context mContext;

    @Override
    public void onCreate() {
        //init context which is shared by other classes
        mContext = getApplicationContext();
        CurrentStationItem.getInstance().setContext(mContext);
        FmUtils.mContext = mContext;
        FmUtils.initConfig(mContext);
    }
}
