package com.android.camera.module;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.Resources.NotFoundException;

import com.android.camera.debug.Log;
import com.android.camera2.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import com.android.camera.util.CameraUtil;

/**
 * Created by SPREADTRUM\matchbox.chang on 17-12-28.
 */

public class ModuleInfoResolve {

    public static class ModuleItem {
        public int moduelId;
        public int visible;
        public int cameraSupprot;
        public int modeSupport;
        //public String nameSpace;
        public String text;
        public String desc;
        public int unSelectIconId;
        public int selectIconId;
        public int coverIconId;
        public int captureIconId;
    }

    private static final Log.Tag TAG = new Log.Tag("ModuleInfoResolve");
    private ArrayList<Integer> mModuleList;
    private ArrayList<Integer> mVisibleModuleList;
    private HashMap<Integer,ModuleItem> mModuleInfo;

    public ModuleInfoResolve(){
        mModuleList = new ArrayList<>();
        mVisibleModuleList = new ArrayList<>();
        mModuleInfo= new HashMap<>();
    }

    private void getModuleInfo(TypedArray moduleRes){
        if(moduleRes == null){
            Log.e(TAG,"resolve module return null");
            return;
        }
        int moduleId = -1;
        ModuleItem item = new ModuleItem();
        moduleId = moduleRes.getInteger(0,-1);
        mModuleList.add(moduleId);
        if(1 == moduleRes.getInteger(1,0)){
            mVisibleModuleList.add(moduleId);
        }
        //init ModuleItem
        item.moduelId = moduleId;
        item.visible = moduleRes.getInteger(1,0);
        item.cameraSupprot = moduleRes.getInteger(2,0);
        item.modeSupport = moduleRes.getInteger(3,0);
        //item.nameSpace = moduleRes.getString(4);
        item.text = moduleRes.getString(5);
        item.desc = moduleRes.getString(6);
        item.unSelectIconId = moduleRes.getResourceId(7,-1);
        item.selectIconId = moduleRes.getResourceId(8,-1);
        item.coverIconId = moduleRes.getResourceId(9,-1);
        item.captureIconId = moduleRes.getResourceId(10,-1);
        mModuleInfo.put(moduleId,item);
    }

    // SPRD: Bug922759 close some feature when 4in1
    private void updateModuleInfo(Context context) {
        int moduleId = 0;
        TypedArray mModuleArray = null;
        if (CameraUtil.isFront4in1Sensor() || CameraUtil.isFrontYUVSensor()) {
            mModuleArray = context.getResources().obtainTypedArray(R.array.tdnr_photo_module);
            try{//sprd:fix bug1159115
                moduleId = mModuleArray.getInteger(0, -1);
                if (moduleId != -1) {
                    ModuleItem item = mModuleInfo.get(moduleId);
                    item.cameraSupprot = context.getResources().getInteger(R.integer.camera_support_back);
                }
            } catch (NotFoundException e) {
                Log.e(TAG,"occur Exception:"+e);
            } finally{
                mModuleArray.recycle();
            }
            mModuleArray = context.getResources().obtainTypedArray(R.array.tdnr_video_module);
            try{
                moduleId = mModuleArray.getInteger(0, -1);
                if (moduleId != -1) {
                    ModuleItem item = mModuleInfo.get(moduleId);
                    item.cameraSupprot = context.getResources().getInteger(R.integer.camera_support_back);
                }
            } catch (NotFoundException e) {
                Log.e(TAG,"occur Exception:"+e);
            } finally{
                mModuleArray.recycle();
            }
        } else if (CameraUtil.isBack4in1Sensor() || CameraUtil.isBackYUVSensor()) {
            mModuleArray = context.getResources().obtainTypedArray(R.array.tdnr_photo_module);
            try {
                moduleId = mModuleArray.getInteger(0, -1);
                if (moduleId != -1) {
                    ModuleItem item = mModuleInfo.get(moduleId);
                    item.cameraSupprot = context.getResources().getInteger(R.integer.camera_support_front);
                }
            } catch (NotFoundException e) {
                Log.e(TAG,"occur Exception:"+e);
            } finally{
                mModuleArray.recycle();
            }
            mModuleArray = context.getResources().obtainTypedArray(R.array.tdnr_video_module);
            try {
                moduleId = mModuleArray.getInteger(0, -1);
                if (moduleId != -1) {
                    ModuleItem item = mModuleInfo.get(moduleId);
                    item.cameraSupprot = context.getResources().getInteger(R.integer.camera_support_front);
                }
            } catch (NotFoundException e) {
                Log.e(TAG,"occur Exception:"+e);
            } finally{
                mModuleArray.recycle();
            }
        }

        updatePortraitModuleInfo(context);
        updateHighResolutionModuleInfo(context);
    }

    // update portrait module
    private void updatePortraitModuleInfo(Context context) {
        int moduleId = -1;
        TypedArray mModuleArray = null;
        mModuleArray = context.getResources().obtainTypedArray(R.array.portrait_photo_module);
        moduleId = mModuleArray.getInteger(0, -1);
        if (moduleId != -1) {
            ModuleItem item = mModuleInfo.get(moduleId);
            item.cameraSupprot = context.getResources().getInteger(R.integer.camera_support_none);
            if(CameraUtil.isPortraitPhotoEnable()){
                item.cameraSupprot = context.getResources().getInteger(R.integer.camera_support_all);
            }else if(CameraUtil.isFrontPortraitPhotoEnable()){
                item.cameraSupprot = context.getResources().getInteger(R.integer.camera_support_front);
            }else if(CameraUtil.isBackPortraitPhotoEnable()){
                item.cameraSupprot = context.getResources().getInteger(R.integer.camera_support_back);
            }
        }
        mModuleArray.recycle();
    }

    // update high resolution module
    private void updateHighResolutionModuleInfo(Context context) {
        int moduleId = -1;
        TypedArray mModuleArray = null;
        mModuleArray = context.getResources().obtainTypedArray(R.array.high_resolution_photo_module);
        moduleId = mModuleArray.getInteger(0, -1);
        if (moduleId != -1) {
            ModuleItem item = mModuleInfo.get(moduleId);
            item.cameraSupprot = context.getResources().getInteger(R.integer.camera_support_none);
            if(CameraUtil.isHighResolutionPhotoEnable()){
                item.cameraSupprot = context.getResources().getInteger(R.integer.camera_support_all);
            }else if(CameraUtil.isFrontHighResolutionPhotoEnable()){
                item.cameraSupprot = context.getResources().getInteger(R.integer.camera_support_front);
            }else if(CameraUtil.isBackHighResolutionPhotoEnable()){
                item.cameraSupprot = context.getResources().getInteger(R.integer.camera_support_back);
            }
        }
        mModuleArray.recycle();
    }

    public void resolve(Context context){
        TypedArray moduleListRes = context.getResources().obtainTypedArray(R.array.module_list);
        TypedArray moduleRes = null;
        if(moduleListRes == null){ //always false ?
            Log.e(TAG,"resolve module list array return null");
            return;
        }
        for(int i = 0;i < moduleListRes.length();i++){
            int moduleResId = moduleListRes.getResourceId(i, -1);
            if(moduleResId < 0){
                continue;
            }
            moduleRes = context.getResources().obtainTypedArray(moduleResId);
            getModuleInfo(moduleRes);
            moduleRes.recycle();
        }
        moduleListRes.recycle();
        updateModuleInfo(context);
    }

    public boolean isModuleVisible(int moduleId){
        ModuleItem item = mModuleInfo.get(moduleId);
        if(item == null){
            Log.e(TAG,"moduleId :"+ moduleId + " is not support");
            return false;
        }
        return 1 == item.visible;
    }

    public String getModuleText(int moduleId){
        ModuleItem item = mModuleInfo.get(moduleId);
        if(item == null){
            Log.e(TAG,"moduleId :"+ moduleId + " is not support");
            return null;
        }
        return item.text;
    }

    public String getModuleDescription(int moduleId){
        ModuleItem item = mModuleInfo.get(moduleId);
        if(item == null){
            Log.e(TAG,"moduleId :"+ moduleId + " is not support");
            return null;
        }
        return item.desc;
    }

    public int getModuleUnselectIcon(int moduleId){
        ModuleItem item = mModuleInfo.get(moduleId);
        if(item == null){
            Log.e(TAG,"moduleId :"+ moduleId + " is not support");
            return -1;
        }
        return item.unSelectIconId;
    }

    public int getModuleSelectIcon(int moduleId){
        ModuleItem item = mModuleInfo.get(moduleId);
        if(item == null){
            Log.e(TAG,"moduleId :"+ moduleId + " is not support");
            return -1;
        }
        return item.selectIconId;
    }

    public int getModuleCoverIcon(int moduleId){
        ModuleItem item = mModuleInfo.get(moduleId);
        if(item == null){
            Log.e(TAG,"moduleId :"+ moduleId + " is not support");
            return -1;
        }
        return item.coverIconId;
    }

    public int getModuleCaptureIcon(int moduleId){
        ModuleItem item = mModuleInfo.get(moduleId);
        if(item == null){
            Log.e(TAG,"moduleId :"+ moduleId + " is not support");
            return -1;
        }
        return item.captureIconId;
    }

    public int getModuleSupportMode(int moduleId){
        ModuleItem item = mModuleInfo.get(moduleId);
        if(item == null){
            Log.e(TAG,"moduleId :"+ moduleId + " is not support");
            return -1;
        }
        return item.modeSupport;
    }

    public int getModuleSupportCamera(int moduleId){
        ModuleItem item = mModuleInfo.get(moduleId);
        if(item == null){
            Log.e(TAG,"moduleId :"+ moduleId + " is not support");
            return -1;
        }
        return item.cameraSupprot;
    }

    public ArrayList<Integer> getModuleIdList(){
        return mModuleList;
    }

    public HashMap<Integer,ModuleItem> getModuleItemList(){
        return mModuleInfo;
    }
}
