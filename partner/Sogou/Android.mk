LOCAL_PATH:= $(call my-dir)

# Sogou input method.
include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := SogouCn
LOCAL_MULTILIB := both
LOCAL_MODULE_STEM := Sogou.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_PATH := $(TARGET_OUT)/vital-app
LOCAL_SRC_FILES := SogouInput_v9.3_Build_2337714_20190924_android_bds_zanrui_ff1.apk
include $(BUILD_PREBUILT)

include $(call all-makefiles-under,$(LOCAL_PATH))
