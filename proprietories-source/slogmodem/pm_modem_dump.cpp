/*
 *  pm_modem_dump.cpp - The cellular MODEM dump class.
 *
 *  Copyright (C) 2015-2017 Spreadtrum Communications Inc.
 *
 *  History:
 *
 *  2015-6-15 Zhang Ziyi
 *  Initial version.
 *
 *  2017-6-3 Zhang Ziyi
 *  Join the Transaction class family.
 */

#include <unistd.h>

#include "cp_dir.h"
#include "cp_set_dir.h"
#include "cp_stor.h"
#include "diag_dev_hdl.h"
#include "multiplexer.h"
#include "parse_utils.h"
#include "pm_modem_dump.h"
#include "wan_modem_log.h"

TransPmModemDump::TransPmModemDump(LogPipeHandler* subsys,
                                   CpStorage& cp_stor,
                                   const struct tm& lt,
                                   const char* dump_path,
                                   const char* prefix,
                                   const char* end_of_content,
                                   uint8_t cmd)
    : TransCpDump{subsys, MEMORY_DUMP, cp_stor, lt, prefix},
      m_dump_path(dump_path),
      m_timer{},
      m_end_of_content{end_of_content},
      m_cmd{cmd},
      dump_ongoing_{},
      silent_wins_{},
      on_going_cb_{} {
  m_end_content_size = strlen(end_of_content);
}

TransPmModemDump::~TransPmModemDump() {
  TransPmModemDump::cancel();
}

int TransPmModemDump::start_dump() {
  // Send command
  int ret {-1};
  uint8_t dump_cmd[2] = {m_cmd, 0xa};
  DiagDeviceHandler* diag = diag_handler();

  ssize_t n = ::write(diag->fd(), dump_cmd, 2);
  if (2 == n) {
    if (open_dump_file()) {
      storage().subscribe_stor_inactive_evt(this, notify_stor_inactive);
      ret = 0;
    }
  }

  return ret;
}

int TransPmModemDump::execute() {
  int err = subsys()->start_diag_trans(*this,
                                       LogPipeHandler::CWS_DUMP);
  if (!err) {
    if (!start_dump()) {  // Start successfully.
      on_started();
      // notify dump ongoing
      if (on_going_cb_) {
        on_going_cb_(subsys());
      }

      TimerManager& tmgr = diag_handler()->multiplexer()->timer_mgr();
      m_timer = tmgr.create_timer(1500, dump_read_check, this);
      err = TRANS_E_STARTED;
    } else {  // Start failed.
      err_log("send command error");
      on_finished(TRANS_R_FAILURE);
      subsys()->stop_diag_trans();
      err = TRANS_E_SUCCESS;
    }
  } else {
    err_log("start diag handler error");
    on_finished(TRANS_R_FAILURE);
    err = TRANS_E_SUCCESS;
  }

  return err;
}

bool TransPmModemDump::process(DataBuffer& buffer) {
  bool ret = false;

  // set the dump status
  dump_ongoing_ = true;
  // if the last frame
  bool found = check_ending(buffer);
  TimerManager& tmgr = diag_handler()->multiplexer()->timer_mgr();

  ssize_t n = 0;
  LogFile* f = dump_file();
  if (f) {
    n = f->write(buffer.buffer + buffer.data_start, buffer.data_len);
  }

  if (static_cast<size_t>(n) != buffer.data_len) {  // Write file failed.
    if (nullptr == f) {
      err_log("dump file no longer exists");
    } else {
      remove_dump_file();
      err_log("need to write %lu, write returns %d",
              static_cast<unsigned long>(buffer.data_len),
              static_cast<int>(n));
    }

    tmgr.del_timer(m_timer);
    m_timer = nullptr;
    storage().unsubscribe_stor_inactive_evt(this);

    subsys()->stop_diag_trans();

    ret = true;

    if (nullptr == m_dump_path) {  // No memory device (/proc/*/mem) to save.
      finish(TRANS_R_FAILURE);
    } else {
      if (save_dump_file()) {
        finish(TRANS_R_SUCCESS);
      } else {
        finish(TRANS_R_FAILURE);
      }
    }
  } else {
    buffer.data_start = 0;
    buffer.data_len = 0;
    if (found) {  // Finishing flag found
      tmgr.del_timer(m_timer);
      m_timer = nullptr;

      close_dump_file();
      storage().unsubscribe_stor_inactive_evt(this);

      subsys()->stop_diag_trans();

      finish(TRANS_R_SUCCESS);
      info_log("dump complete");
      ret = true;
    }
  }

  return ret;
}

void TransPmModemDump::dump_read_check(void* param) {
  TransPmModemDump* dump = static_cast<TransPmModemDump*>(param);

  dump->m_timer = nullptr;

  // Notify ongoing
  if (dump->on_going_cb_) {
    dump->on_going_cb_(dump->subsys());
  }

  // Check whether silent period is too long.
  if (dump->dump_ongoing_) {
    dump->silent_wins_ = 0;
  } else {
    ++dump->silent_wins_;
    if (dump->silent_wins_ >= 4) {  // Max 4 windows (6 seconds)
      err_log("read timeout, %lu bytes read",
              static_cast<unsigned long>(dump->dump_file()->size()));

      dump->remove_dump_file();
      dump->storage().unsubscribe_stor_inactive_evt(dump);

      dump->subsys()->stop_diag_trans();

      int result {TRANS_R_FAILURE};

      if (dump->m_dump_path && dump->save_dump_file()) {
        result = TRANS_R_SUCCESS;
      }
      dump->finish(result);
      return;
    }
  }

  dump->dump_ongoing_ = false;

  TimerManager& tmgr = dump->diag_handler()->multiplexer()->timer_mgr();
  dump->m_timer = tmgr.create_timer(1500, dump_read_check, dump);
}

bool TransPmModemDump::check_ending(DataBuffer& buffer) {
  uint8_t* src_ptr = buffer.buffer + buffer.data_start;
  size_t src_len = buffer.data_len;
  uint8_t* dst_ptr;
  size_t dst_len;
  size_t read_len;
  bool has_frame;
  bool ret = false;

  while (src_len) {
    has_frame =
        parser_.unescape(src_ptr, src_len, &dst_ptr, &dst_len, &read_len);
    src_len -= read_len;
    src_ptr += read_len;
    if (has_frame) {
      uint8_t* data_ptr = parser_.get_payload(dst_ptr);
      size_t data_len = dst_len - parser_.get_head_size();
      if (data_len == m_end_content_size &&
          !memcmp(data_ptr, m_end_of_content, data_len)) {
        ret = true;
      }
    }
  }
  return ret;
}

void TransPmModemDump::notify_stor_inactive(void *client, unsigned priority) {
  TransPmModemDump* dump = static_cast<TransPmModemDump*>(client);

  if (TS_EXECUTING == dump->state()) {
    if (!dump->dump_file() ||
        (dump->dump_file() &&
         priority == dump->dump_file()->dir()->cp_set_dir()->priority())) {
      dump->storage().unsubscribe_stor_inactive_evt(dump);
      dump->cancel();
      dump->finish(TRANS_R_FAILURE);
    }
  }
}

bool TransPmModemDump::copy_dump_content(const char* mem_name,
                                         const char* mem_path) {
  LogFile* mem_file = open_dump_mem_file(mem_name);
  bool ret = false;
  if (mem_file) {
    if (mem_file->copy(mem_path)) {
      err_log("dump proc memory: %s failed.", mem_path);
    } else {
      ret = true;
      info_log("dump proc memory: %s successfully.", mem_path);
    }
    mem_file->close();
  } else {
    err_log("create %s dump file failed", mem_name);
  }

  return ret;
}

bool TransPmModemDump::save_dump_file() {
  bool ret = false;

  ret = copy_dump_content("memory", m_dump_path);

  if (!access(AON_IRAM_FILE, F_OK)) {
    copy_dump_content("aon-iram", AON_IRAM_FILE);
  }
  if (!access(PUBCP_IRAM_FILE, F_OK)) {
    copy_dump_content("pubcp-iram", PUBCP_IRAM_FILE);
  }

  return ret;
}

void TransPmModemDump::cancel() {
  if (TS_EXECUTING == state()) {
    TimerManager& tmgr = diag_handler()->multiplexer()->timer_mgr();
    tmgr.del_timer(m_timer);
    m_timer = nullptr;

    remove_dump_file();
    storage().unsubscribe_stor_inactive_evt(this);

    subsys()->stop_diag_trans();

    on_canceled();
  }
}
