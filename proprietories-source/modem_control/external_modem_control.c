/**
 * extern_modem_control.c ---
 *
 * Copyright (C) 2018 Spreadtrum Communications Inc.
 */
#include <cutils/properties.h>
#include <pthread.h>

#include "modem_control.h"
#include "modem_load.h"
#include "modem_connect.h"
#include "modem_io_control.h"

void modem_ctrl_reboot_all_system(void) {
  modem_reboot_all_modem();
}

/* loop detect sipc modem state */
void *modem_ctrl_listen_modem(void *param) {
  char assert_dev[PROPERTY_VALUE_MAX] = {0};
  int ret, fd = -1;
  fd_set rfds;
  char buf[256];
  int numRead, numWrite;
  pthread_t t1;
  MODEM_LOGD("Enter %s !", __FUNCTION__);

  /* get diag assert prop */
  property_get(ASSERT_DEV_PROP, assert_dev, "not_find");
  MODEM_LOGD("%s: %s = %s\n", __FUNCTION__, ASSERT_DEV_PROP, assert_dev);

  pthread_t tid;
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

  /*set up socket connection to clients*/
  if (pthread_create(&tid, &attr, (void*)modem_setup_clients_connect, NULL) < 0) {
      MODEM_LOGE("Failed to create modemd listen accept thread");
  } else {
      /* create listen_thread */
      if (0 != pthread_create(&t1, NULL, modem_ctrl_listen_clients, NULL)) {
        MODEM_LOGE(" %s: create error!\n", __FUNCTION__);
      }
  }

  /* open assert_dev */
  while ((fd = open(assert_dev, O_RDWR)) < 0) {
    MODEM_LOGE("%s: open %s failed, error: %s\n", __FUNCTION__, assert_dev,
               strerror(errno));
    sleep(1);
  }
  MODEM_LOGD("%s: open assert dev: %s, fd = %d\n", __FUNCTION__, assert_dev,
             fd);

  FD_ZERO(&rfds);
  FD_SET(fd, &rfds);

  /*listen assert */
  for (;;) {
    MODEM_LOGD("%s: wait for modem assert/hangup event ...", __FUNCTION__);
    do {
       ret = select(fd + 1, &rfds, NULL, NULL, 0);
    } while (ret == -1 && errno == EINTR);
    if (ret > 0) {
      /* get wake_lock */
      modem_ctrl_enable_wake_lock(1, __FUNCTION__);
      memset(buf, 0, sizeof(buf));
      MODEM_LOGD("%s: enter read ...", __FUNCTION__);
      numRead = read(fd, buf, sizeof(buf));
      if (numRead <= 0) {
        MODEM_LOGE("%s: read %d return %d, errno = %s", __FUNCTION__, fd,
                   numRead, strerror(errno));
        sleep(1);
        continue;
      }

      MODEM_LOGD("%s: buf=%s", __FUNCTION__, buf);

      if (strstr(buf, MODEM_RESET)) {
          MODEM_LOGD("read Modem Reset from modem\n");
          modem_ctrl_set_wait_reset_flag(0);
      }else if (strstr(buf, "HUNG")) {
         char prop[PROPERTY_VALUE_MAX];
         int is_reset;

         memset(prop, 0, sizeof(prop));
         property_get(MODEM_RESET_PROP, prop, "0");
         is_reset = atoi(prop);
         MODEM_LOGD("%s = %s, is reset = %d\n", MODEM_RESET_PROP, prop, is_reset);

         if (is_reset) {
             MODEM_LOGD("%s: modem panic, reboot system\n", __FUNCTION__);
             modem_ctrl_reboot_all_system();
         }
      }

      if (modem_ctrl_get_boot_mode() != BOOT_MODE_CALIBRATION) {
        numWrite = modem_write_data_to_clients(buf, numRead);
        MODEM_LOGD("%s: write to modemd len = %d\n", __FUNCTION__, numWrite);
      }
      /* release wake_lock */
      modem_ctrl_enable_wake_lock(0, __FUNCTION__);
    }
  }
  return NULL;
}

